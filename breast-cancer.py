import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from keras import models
from keras import layers
from keras.utils import to_categorical


df = pd.read_csv("data.csv")
df = df.drop("id", axis=1)
df = df.drop("Unnamed: 32", axis=1)

features = df.columns[1:]
labels = df.columns[0]

y = []
ndata = range(len(df[labels]))

for i in ndata:
    if df[labels][i] == "M":
        y.append(0)
    else:
        y.append(1)

for i in features:
    df[i] = (df[i] - df[i].mean()) / df[i].std()

df = df.drop("diagnosis", axis=1)
x = pd.DataFrame.to_numpy(df)
x = np.array(x).astype(np.float32)
y = to_categorical(y)

x_train = x[:400]
y_train = y[:400]
x_test = x[400:]
y_test = y[400:]

model = models.Sequential()
model.add(layers.Dense(128, activation="relu", input_shape=(30,)))
model.add(layers.Dense(128, activation="relu"))
model.add(layers.Dense(128, activation="relu"))
model.add(layers.Dense(128, activation="relu"))
model.add(layers.Dense(2, activation="sigmoid"))

model.compile(optimizer="rmsprop", loss="binary_crossentropy", metrics=["accuracy"])

history = model.fit(x_train, y_train, epochs=50, batch_size=460, validation_data=(x_test, y_test))

val_acc = history.history["val_acc"]
acc = history.history["acc"]
epochs = range(1,len(acc)+1)
plt.plot(epochs, acc, "blue", label="Train")
plt.plot(epochs, val_acc, "green", label="Validation")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.show()